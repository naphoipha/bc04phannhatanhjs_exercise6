// start ex1
function sumNumbers(){
    console.log("yes");
    var num = 0;
    var sum = 0;

    while (sum < 10000) {
        num++;
        sum = sum + num;
        var result = "Smallest number: " + num;
    }

    document.getElementById("showResult1").innerHTML =result;

}
// end ex1

// start ex2
function calEx2(){
    var xNumber;
    var nNumber;
    var mul = 1;
    var sum = 0;

    xNumber = document.getElementById("txt-x-number").value*1;
    nNumber = document.getElementById("txt-n-number").value*1;

    for (var i = 1; i <= nNumber; i++){
        mul = mul * xNumber;
        sum = sum + mul; 
        var result2 = "Sum of numbers: " + sum;
    }
    document.getElementById("showResult2").innerHTML =result2;
    console.log({sum});
}
// end ex2

// start ex3
function factorial(n){
    var fact = 1;
    if (n == 0 || n == 1) { 
        return fact;
    } else {
        for (var i = 2 ; i <= n ; i++){
            fact *= i;
        }
        return fact;
    }
}

function calEx3(){
    var inputNum;
    inputNum = document.getElementById("txt-n-factorial-number").value*1;
    var result3;

    result3 = factorial(inputNum);
    document.getElementById("showResult3").innerHTML = result3;

    console.log({result3});
}
// end ex3

// start ex4
function changeEx4(){
    var divs = document.getElementsByClassName("change");
    for (var i = 0; i < divs.length; i++){
        if ((i + 1) % 2 == 0){
            divs[i].style.background = "red";
        }
        else {
            divs[i].style.background = "blue";
        }
    }
}
// end ex4

//ex 5 chỉ in ra console log
// start ex5
function calEx5(){
    var numInput;
    numInput = document.getElementById("txt-n-prime-number").value*1;

    for(var i = 1; i <= numInput; i++){
        var flag = 0;
        for (var j = 2; j < i; j++){
            if (i%j == 0){
                flag = 1;
                break;
            }
        }
        if (i > 1 && flag == 0) {
            console.log(i);
        }
    }
    var result5;
    document.getElementById("showResult5").innerHTML = result5;
}
// end ex5